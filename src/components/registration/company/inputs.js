export default {
  company: [
    {
      title: 'Bolagsnamn',
      type: 'text',
      placeholder: 'Företag AB',
      name: 'company_name',
      model: 'company_name'
    },
    {
      title: 'Land',
      type: 'text',
      placeholder: 'Sverige',
      name: 'country',
      model: 'country'
    },
    {
      title: 'Typ av företag',
      type: 'text',
      placeholder: 'Webbshop, fysisk butik, lager',
      name: 'type',
      model: 'type'
    },
    {
      title: 'Hemsida',
      type: 'text',
      placeholder: 'https://www.foretag.se',
      name: 'company_website',
      model: 'company_website'
    }
  ],
  user: [
    {
      title: 'Förnamn',
      type: 'text',
      placeholder: '',
      name: 'firstname',
      model: 'firstname'
    },
    {
      title: 'Efternamn',
      type: 'text',
      placeholder: '',
      name: 'lastname',
      model: 'lastname'
    },
    {
      title: 'Epost',
      type: 'email',
      placeholder: 'info@foretag.se',
      name: 'email',
      model: 'email'
    },
    {
      title: 'Lösenord',
      type: 'password',
      placeholder: '**********',
      name: 'password',
      model: 'password'
    }
  ]
}
