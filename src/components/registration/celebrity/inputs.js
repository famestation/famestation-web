export default {
  about: [
    {
      title: 'Förnamn',
      placeholder: 'Kim',
      type: 'text',
      name: 'firstname',
      model: 'firstname'
    },
    {
      title: 'Efternamn',
      placeholder: 'Warth',
      type: 'text',
      name: 'lastname',
      model: 'lastname'
    },
    {
      title: 'Alias',
      placeholder: 'Kim3000',
      type: 'text',
      name: 'alias',
      model: 'alias'
    },
    {
      title: 'URL till bild',
      type: 'text',
      name: 'profile_picture_url',
      model: 'profile_picture_url'
    }
  ],
  miscellaneous: [
    {
      title: 'Epost',
      placeholder: 'kim_warth@gmail.com',
      type: 'email',
      name: 'email',
      model: 'email'
    },
    {
      title: 'Lösenord',
      placeholder: '********',
      type: 'password',
      name: 'password',
      model: 'password'
    }
  ]
}
