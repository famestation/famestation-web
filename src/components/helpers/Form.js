import Errors from '../helpers/Errors'
import axios from 'axios'

export default class Form {

  constructor (data) {
    this.originalData = data
    this.errors = new Errors()
    for (const field in data) {
      this[field] = data[field]
    }
  }

  reset () {
    for (const field in this.originalData) {
      this[field] = ''
    }
  }

  data () {
    const data = Object.assign({}, this)
    delete data.errors
    delete data.originalData

    return data
  }

  submit (requestType, url) {
    axios[requestType](url, this.data())
    .then(this.onSuccess.bind(this))
    .catch(this.onFail.bind(this))
  }

  onSuccess (response) {
    this.errors.clear()
    this.reset()
  }

  onFail (error) {
    this.errors.record(error.response.data.result.error_message)
  }

}
