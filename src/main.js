// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import routes from './routes'
import store from './vuex/store'
import axios from 'axios'
import middlewares from './middlewares'

var headers = require('./utils/config')

window.axios = axios
window.axios.defaults.headers.common = headers.getAuthHeaders()
Vue.use(VueRouter)

export const router = new VueRouter({
  mode: 'hash',
  base: __dirname,
  linkActiveClass: 'is-active',
  routes
})

router.beforeEach(middlewares.isAuthed)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
