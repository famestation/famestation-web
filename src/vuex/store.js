import Vue from 'vue'
import Vuex from 'vuex'

const debug = process.env.NODE_ENV !== 'production'
Vue.use(Vuex)

/**
 * Vuex modules
 */
import login from './modules/login/store'
import companies from './modules/companies/store'
import celebrities from './modules/celebrities/store'
import chat from './modules/chat/store'
import misc from './modules/misc/store'

export default new Vuex.Store({
  strict: debug,
  modules: {
    login,
    companies,
    celebrities,
    misc,
    chat
  }
})
