export default {
  isLoading: state => state.loading,
  loginErrors: state => state.errors,
  getCompanies: state => state.companies,
  getCompany: state => state.company
}
