import axios from 'axios'
import api from '../../../utils/api'

export default {

  async fetchCompanies ({ commit }) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.get(`${api.v1_url}/companies`)
      commit('SET_LOADING', false)
      commit('UPDATE_COMPANIES', response.data.result)
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },
  async fetchCompany ({ commit }, companyId) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.get(`${api.v1_url}/companies/${companyId}`)
      commit('SET_LOADING', false)
      commit('UPDATE_COMPANY', response.data.result)
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },

  async createCompany ({ commit }, company) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.post(`${api.v1_url}/companies`, company)
      commit('SET_LOADING', false)
      return response
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },

  async updateCompany ({ commit }, companyData) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.put(`${api.v1_url}/companies/${companyData.id}`, companyData)
      commit('SET_LOADING', false)
      return response
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },
  async setError ({ commit }, errors) {
    commit('SET_ERROR', errors)
  }

}
