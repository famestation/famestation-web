export default {
  SET_LOADING: (state, status) => {
    state.loading = status
  },

  SET_ERROR: (state, errors) => {
    state.errors = errors
  },

  UPDATE_COMPANIES: (state, companies) => {
    state.companies = companies
  },

  UPDATE_COMPANY: (state, company) => {
    state.company = company
  }
}
