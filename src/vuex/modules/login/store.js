import axios from 'axios'
import settings from '../../../utils/settings'
import jwt from 'jwt-decode'

const state = {
  authUser: null,
  loading: false
}

const getters = {
  userType: state => {
    if (state.authUser) {
      return jwt(state.authUser)._type
    }
    return state.authUser
  },
  userID: state => {
    if (state.authUser) {
      return jwt(state.authUser)._id
    }
    return state.authUser
  },
  isAuthed: state => state.authUser ? true : false,
  isLoading: state => state.loading
}

const mutations = {
  SET_AUTH_USER: (state, token) => {
    window.localStorage.setItem('token', token)
    state.authUser = token
  },
  SET_TYPE_OF_USER: (state, type) => {
    state.userType = type
  },

  SET_LOADING: (state, status) => {
    state.loading = status
  }
}

const actions = {
  async login ({ commit }, credentials) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.post(`${settings.api_url}/auth/login`, credentials)
      commit('SET_LOADING', false)
      commit('SET_AUTH_USER', response.data.result)
      return response
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },
  setTypeOfUser: ({ commit }, type) => commit('SET_TYPE_OF_USER', type),
  setAuthenticatedUser: ({ commit }, userObj) => commit('SET_AUTH_USER', userObj),
  getAuthenticatedUser: ({ commit }) => commit('GET_AUTH_USER')
}

const module = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}

export default module
