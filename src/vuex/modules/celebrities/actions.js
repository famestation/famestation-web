import axios from 'axios'
import api from '../../../utils/api'

export default {

  async fetchCelebrities ({ commit }) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.get(`${api.v1_url}/celebrities`)
      commit('SET_LOADING', false)
      commit('UPDATE_CELEBRITIES', response.data.result)
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },
  async fetchCelebrity ({ commit }, celebrityId) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.get(`${api.v1_url}/celebrities/${celebrityId}`)
      commit('SET_LOADING', false)
      commit('UPDATE_CELEBRITY', response.data.result)
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },

  async createCelebrity ({ commit }, celebrity) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.post(`${api.v1_url}/celebrities`, celebrity)
      commit('SET_LOADING', false)
      commit('UPDATE_ERRORS', false)
      return response
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },

  async updateCelebrity ({ commit }, celebrityData) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.put(`${api.v1_url}/celebrities/${celebrityData.id}`, celebrityData)
      commit('SET_LOADING', false)
      commit('UPDATE_ERRORS', false)
      return response
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },
  setError ({ commit }, errors) {
    commit('UPDATE_ERRORS', errors)
  }

}
