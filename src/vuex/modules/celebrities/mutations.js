export default {
  SET_LOADING: (state, status) => {
    state.loading = status
  },
  UPDATE_CELEBRITIES: (state, data) => {
    state.celebrities = data
  },
  UPDATE_ERRORS: (state, data) => {
    state.errors = data
  },
  UPDATE_CELEBRITY: (state, celebrity) => {
    state.celebrity = celebrity
  }
}
