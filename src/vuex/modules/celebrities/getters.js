export default {
  getCelebrities: (state) => state.celebrities,
  isLoading: (state) => state.loading,
  errors: (state) => state.errors,
  getCelebrity: (state) => state.celebrity
}
