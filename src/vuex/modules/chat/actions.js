import axios from 'axios'
import api from '../../../utils/api'

export default {
  async fetchCelebrityConversations ({ commit }, id) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.get(`${api.v1_url}/celebrities/${id}/conversations`)
      commit('SET_LOADING', false)
      commit('UPDATE_CONVERSATIONS', response.data.result)
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },
  async fetchCompanyConversations ({ commit }, id) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.get(`${api.v1_url}/companies/${id}/conversations`)
      commit('SET_LOADING', false)
      commit('UPDATE_CONVERSATIONS', response.data.result)
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },

  async fetchMessages ({ commit }, id) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.get(`${api.v1_url}/conversations/${id}`)
      commit('SET_LOADING', false)
      commit('UPDATE_MESSAGES', response.data.result)
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },

  async createConversation ({ commit }, conversation) {
    try {
      commit('SET_LOADING', true)
      const response = await axios.post(`${api.v1_url}/conversations`, conversation)
      commit('SET_LOADING', false)
      commit('UPDATE_CONVERSATIONS', response.data.result)
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  },

  async updateMessages ({ commit }, message) {
    try {
      commit('SET_LOADING', true)
      commit('FILL_MESSAGES', message)
      commit('SET_LOADING', false)
    } catch (error) {
      commit('SET_LOADING', false)
      throw error
    }
  }

}
