export default {
  SET_LOADING: (state, status) => {
    state.loading = status
  },
  UPDATE_CONVERSATIONS: (state, data) => {
    state.conversations = data
  },
  UPDATE_ERRORS: (state, data) => {
    state.errors = data
  },
  UPDATE_MESSAGES: (state, data) => {
    state.messages = data
  },
  FILL_MESSAGES: (state, data) => {
    state.messages.messages.push(data)
  }
}
