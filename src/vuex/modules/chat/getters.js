export default {
  isLoading: state => state.loading,
  loginErrors: state => state.errors,
  getConversations: state => state.conversations,
  getMessages: state => state.messages
}
