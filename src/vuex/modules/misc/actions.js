import axios from 'axios'
import api from '../../../utils/api'

export default {

  async fetchCategories ({ commit }, companyId) {
    try {
      const response = await axios.get(`${api.v1_url}/categories`)
      commit('SET_CATEGORIES', response.data.result)
    } catch (error) {
      commit('SET_LOADING', false)
      throw error.response.data.result
    }
  }

}
