import middlewares from './middlewares'
import Home from './components/Home'
import Login from './components/Login'
import Registration from './components/registration/Registration'
import Request from './components/Request.vue'
import About from './components/About'

import BetaRegistration from './components/BetaRegistration.vue'

import Celebrities from './components/company-view/Celebrities'
import Celebrity from './components/company-view/CelebrityProfile.vue'

import Profile from './components/profile/Profile'
import EditProfile from './components/profile/EditProfile'

import Companies from './components/celebrity-view/Companies'
import Company from './components/celebrity-view/CompanyProfile'

import Chat from './components/chat/Chat.vue'

export default [
  {
    path: '/',
    component: Home,
    name: 'home'
  },
  {
    path: '/login',
    component: Login,
    name: 'login'
  },
  {
    path: '/celebrities',
    component: Celebrities,
    name: 'celebrities',
    meta: { requiresAuth: true, visibleTo: 'company' },
    beforeEnter: middlewares.hasAccess
  },
  {
    path: '/celebrities/:id',
    component: Celebrity,
    name: 'celebrity',
    meta: { requiresAuth: true, visibleTo: 'company' },
    beforeEnter: middlewares.hasAccess
  },
  {
    path: '/companies',
    component: Companies,
    name: 'companies',
    meta: { requiresAuth: true, visibleTo: 'celebrity' },
    beforeEnter: middlewares.hasAccess
  },
  {
    path: '/companies/:id',
    component: Company,
    name: 'company',
    meta: { requiresAuth: true, visibleTo: 'celebrity' },
    beforeEnter: middlewares.hasAccess
  },
  {
    path: '/about',
    component: About,
    name: 'about'
  },
  {
    path: '/request',
    component: Request,
    name: 'request'
  },
  {
    path: '/registration',
    component: Registration,
    name: 'registration'
  },
  {
    path: '/beta',
    component: BetaRegistration,
    name: 'beta'
  },
  {
    path: '/profile/:id',
    component: Profile,
    name: 'profile',
    meta: { requiresAuth: true }
  },
  {
    path: '/profile/updateProfile/:id',
    component: EditProfile,
    name: 'update',
    meta: { requiresAuth: true }
  },
  {
    path: '/chat',
    component: Chat,
    name: 'chat',
    meta: { requiresAuth: true }
  }
]
