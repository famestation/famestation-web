export default (to, from, next) => {
  if (to.meta.requiresAuth) {
    if (window.localStorage.getItem('token')) {
      return next()
    }
    return next({ name: 'login' })
  }
  return next()
}
