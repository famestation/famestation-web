import isAuthed from './isAuthed'
import hasAccess from './hasAccess'

export default {
  isAuthed,
  hasAccess
}
