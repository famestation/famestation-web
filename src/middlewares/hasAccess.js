import jwtDecode from 'jwt-decode'

export default (to, from, next) => {
  const token = window.localStorage.getItem('token')
  const type = jwtDecode(token)._type

  if (to.meta.visibleTo === type) {
    return next()
  }

  return next({ name: 'home' })
}
