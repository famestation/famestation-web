export const getAuthHeaders = () => {
  const token = window.localStorage.getItem('token')
  return {
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + token
  }
}

export const token = () => {
  return window.localStorage.getItem('token')
}
